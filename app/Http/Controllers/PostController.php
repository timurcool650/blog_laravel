<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommRequest;
use App\Http\Requests\CreatePostRequest;
use App\Models\Comment;
use App\Models\Post;
use App\Models\PostImages;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;



class PostController extends Controller
{
    public function index(Post $post)
    {
        $posts = Post::paginate(6);
        return view('post.index', [
            'posts'=>$posts,
            'post'=>$post
        ]);
    }

    public function create(Post $post)
    {
        $this->authorize('create', $post);
        return view('post.create');
    }

    public function store(CreatePostRequest $request, Post $post, PostImages $postImages)
    {
        $this->authorize('create', $post);
        $data=$request->validated();

        $post = Post::query()->
        create($data);

        foreach ($request->file('path') as $image) {
            $path=$image->store('public/images');
            PostImages::create([
                'post_id'=>$post->id,
                'path'=>$path
            ]);
        }
        return redirect()->route('post.show', $post);
    }

    public function storeComment(CommRequest $request, Post $post)
    {
        $validated=$request->validated();
        Comment::create($validated);
        return redirect()->route('post.show', $post);
    }


    public function show(Post $post, User $user)
    {
        $comment=Comment::all();
        $images=PostImages::all();
        return view('post.show', [
            'post'=>$post,
            'user'=>$user,
            'comment'=>$comment,
            'images'=>$images
        ]);
    }

    public function edit(Post $post)
    {
        $this->authorize('update', $post);
        return view('post.edit', [
            'post'=>$post
        ]);
    }

    public function update(CreatePostRequest $request, Post $post)
    {
        $this->authorize('update', $post);
        $validated=$request->validated();
        $post->update($validated);
        return redirect()->route('post.show', $post);
    }

    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);
        $images=PostImages::all();

        try {
            DB::beginTransaction();

            foreach ($images as $image) {
                if ($image->post_id === $post->id) {
                    $image->delete();
                    Storage::delete($image->path);
                }
            }
            $comments=Comment::all();
            foreach ($comments as $comment) {
                if ($comment->post_id === $post->id) {
                    $comment->delete();
                }
            }

            $post->delete();

            DB::commit();
        }
        catch (Throwable){
            DB::rollBack();
        }

        return redirect()->route('post');
    }

}
