<?php

namespace App\Http\Requests;
use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class CreatePostRequest extends FormRequest
{

    public function rules()
    {
        return [
            'user_id' => 'required',
            'title' =>'required|max:255',
            'description'=>'required|max:255',
            'content'=>'required',
            'path' => 'array',
            'path.*' => 'required|image|mimes:jpg,png,jpeg,gif,svg',
        ];
    }
}
