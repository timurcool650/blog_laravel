<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class Admin
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return $user->is_admin == 1;
    }


    public function update(User $user)
    {
        return $user->is_admin == 1;
    }


    public function delete(User $user)
    {
        return $user->is_admin == 1;
    }
}
