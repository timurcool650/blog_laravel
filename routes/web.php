<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [HomeController::class, 'index'])->middleware('auth')->name('home');

Route::patch('/', [HomeController::class, 'update'])->middleware('auth')->name('home.update');

Route::get('/posts', [PostController::class, 'index'])->middleware('auth')->name('post');

Route::get('/createPost', [PostController::class, 'create'])->middleware('auth')->name('post.create');

Route::post('/posts', [PostController::class, 'store'])->middleware('auth')->name('post.store');

Route::get('/posts/{post}', [PostController::class, 'show'])->middleware('auth')->name('post.show');

Route::post('/posts/{post}', [PostController::class, 'storeComment'])->middleware('auth')->name('comment.store');

Route::get('/posts/{post}/edit', [PostController::class, 'edit'])->middleware('auth')->name('post.edit');

Route::patch('/posts/{post}', [PostController::class, 'update'])->middleware('auth')->name('post.update');

Route::delete('/posts/{post}', [PostController::class, 'destroy'])->middleware('auth')->name('post.destroy');
