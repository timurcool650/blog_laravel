<x-home>
    <div>
        <a href="{{route('post')}}" class="btn btn-secondary mb-3">Назад</a>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-img-top d-flex flex-wrap">
                    @foreach($images as $image)
                        @if($image->post_id === $post->id)
                        <img src="{{ Storage::url($image->path)}}" style="width: 25rem; margin: 2%">
                        @endif
                    @endforeach
                </div>
                <div class="card-body">
                    <h4 class="card-title">{{$post->title}}</h4>
                    <p class="card-text">{{$post->description}}</p>
                    <p class="card-text">{{$post->content}}</p>
                </div>
            </div>
            <div class="mt-3">
                @can('update', $post)
                <a class="btn btn-primary" href="{{route('post.edit', $post->id)}}">Редактировать пост</a>
                @endcan
                @can('delete', $post)
                <form action="{{route('post.destroy', $post->id)}}" method="post" class="mt-2">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger" type="submit">Удалить пост</button>
                </form>
                    @endcan
            </div>
            <form action="{{route('comment.store', $post->id)}}" method="post">
                @csrf
                <div class="mb-3">
                    <label for="content" class="form-label mt-3"><h4>Оставить комментарий: </h4></label>
                    <textarea class="form-control border border-dark" id="content" name="content"></textarea>
                </div>
                <div style="display: none">
                    <input value="{{$post->id}}" readonly name="post_id" id="post_id">
                </div>
                <button type="submit" class="btn btn-success">Добавить</button>
            </form>
            <div>
                <h5 class="mt-3">Оставленные комментарии: </h5>
                @foreach($comment as $comm)
                    @if($comm->post_id === $post->id)
                        <div class="card border-dark mb-3" style="max-width: 30rem;">
                            <div class="card-body">
                                <p class="card-text">{{$comm->content}}</p>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</x-home>
