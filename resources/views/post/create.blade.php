<x-home>
    {{ json_encode($errors->all()) }}

    <form action="{{route('post.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="text" class="form-control visually-hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}" readonly>
        <div class="mb-3">
            <label for="title" class="form-label">Заголовок поста</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Описание поста</label>
            <textarea class="form-control" id="description" name="description"></textarea>
        </div>
        <div class="mb-3">
            <label for="content" class="form-label">Контент поста</label>
            <textarea class="form-control" id="content" name="content"></textarea>
        </div>
        <div class="mb-3">
            <label for="image" class="form-label">Добавьте изображение</label>
            <input class="form-control" type="file" id="image" name="path[]" multiple>
        </div>
        <button type="submit" class="btn btn-primary">Создать</button>
    </form>
</x-home>
