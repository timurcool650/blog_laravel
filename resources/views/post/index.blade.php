<x-home>
    @can('create', $post)
        <div>
            <a href="{{route('post.create')}}" class="btn btn-success mb-3">Создать пост</a>
        </div>
    @endcan
    <div class="d-flex justify-content-xl-start flex-wrap">
        @foreach($posts as $post)
            <div class="card" style="width: 18rem; margin-right: 3%; margin-bottom: 3%">
                <div class="card-body">
                    <h4 class="card-title">{{$post->title}}</h4>
                    <p class="card-text">{{$post->description}}</p>
                    <a href="{{route('post.show', $post)}}" class="btn btn-primary">Перейти к посту</a>
                </div>
            </div>
        @endforeach
    </div>
    {{ $posts->links() }}
</x-home>
