<x-home>
    <a href="{{route('post.show', $post)}}" class="btn btn-secondary mb-3">Назад</a>
    <form action="{{route('post.update', $post->id)}}" method="post">
        @csrf
        @method('patch')
        <input type="text" class="form-control visually-hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}" readonly>
        <div class="mb-3">
            <label for="title" class="form-label">Изменить заголовок поста</label>
            <input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Изменить описание поста</label>
            <textarea class="form-control" id="description" name="description">{{$post->description}}</textarea>
        </div>
        <div class="mb-3">
            <label for="content" class="form-label">Изменить контент поста</label>
            <textarea class="form-control" id="content" name="content">{{$post->content}}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Обновить</button>
    </form>


</x-home>


















