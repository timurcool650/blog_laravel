<x-home>
    <div class="row">
        <div class="col-5">
            <h2>Добро пожаловать, {{\Illuminate\Support\Facades\Auth::user()->name}}</h2></div>
    </div>
    <a class="btn btn-success" href="{{route('post')}}">Перейти к постам</a>
</x-home>
